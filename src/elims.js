var states = {
  menu: true,
  item: 0,
  about: false,
  levels: false,
  play: false,
  focus: null,
  first: -1,
  second: -1,
  solved: false,
}
var colors = {
  dark: [10, 18, 29],
  light: [70, 193, 164],
  blue: [12, 130, 171],
  yellow: [253, 210, 69],
  green: [129, 190, 88],
  purple: [31, 34, 105],
}
var sounds = {
  bool: true,
  skip: null,
  enter: null,
  quit: null,
  select: null,
  swap: null,
  solved: null,
}
var levels = {
  levels: [
    [0, 2, 3, 1],
    [0, 3, 4, 1, 2],
    [2, 4, 0, 3, 5, 1],
    [5, 3, 2, 1, 4, 0, 6],
    [6, 4, 2, 3, 5, 1, 0, 7],
    [0, 1, 8, 3, 2, 5, 4, 7, 6],
    [7, 4, 8, 9, 2, 5, 6, 0, 1, 3],
    [5, 4, 8, 7, 1, 10, 6, 3, 2, 9, 0]
  ],
  temp: null,
  selection: create2DArray(3, 3, 0, true),
  state: create2DArray(3, 3, 0, true),
  active: null,
  selected: [0, 0],
  states: [
    [3, 3, 3, 3],
    [3, 3, 3, 3, 3],
    [3, 3, 3, 3, 3, 3],
    [3, 3, 3, 3, 3, 3, 3],
    [3, 3, 3, 3, 3, 3, 3, 3],
    [3, 3, 3, 3, 3, 3, 3, 3, 3],
    [3, 3, 3, 3, 3, 3, 3, 3, 3, 3],
    [3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3]
  ],
}
var texts = {
  font: null,
  title: 'ELIMS',
  menu: ['START', 'ABOUT'],
  credit: 'AHARONOFF 2020',
  about: 'ELIMS\nIS A PUZZLE GAME,\nWHERE THE GOAL IS\nTO MAKE EVERYBODY\nHAPPY.\n\nYOU KNOW,\nLIKE IN REAL LIFE.\n\n\n\nACTUALLY YOU CANNOT\nMAKE EVERYBODY HAPPY\nTHERE, BUT AT LEAST\nYOU CAN DO IT IN ELIMS.',
  large: null,
  medium: null,
  small: null,
}
var board = {
  size: null,
  center: null,
}

function preload() {
  texts.font = loadFont('font/orbitron1.ttf')
  sounds.skip = loadSound('sound/skip.wav')
  sounds.enter = loadSound('sound/enter.wav')
  sounds.quit = loadSound('sound/quit.wav')
  sounds.select = loadSound('sound/select.wav')
  sounds.swap = loadSound('sound/swap.wav')
  sounds.solved = loadSound('sound/solved.wav')
}

function setup() {
  createCanvas(windowWidth, windowHeight)
  board.size = round(windowHeight * 0.9)
  board.center = round(windowHeight * 0.9 * 0.5)
  texts.large = round(windowHeight * 0.175)
  texts.medium = round(windowHeight * 0.1)
  texts.small = round(windowHeight * 0.05)
}

function draw() {
  createCanvas(board.size, board.size)
  colorMode(RGB, 255, 255, 255, 1)
  rectMode(CENTER)
  textAlign(CENTER)
  textFont(texts.font)
  background(colors.light)
  noFill()
  stroke(colors.purple)
  strokeWeight(board.size * 0.025)
  rect(board.center, board.center, board.size * 0.95, board.size * 0.95)
  // menu
  if (states.menu === true) {
    // title
    stroke(colors.dark)
    strokeWeight(board.size * 0.05)
    textSize(texts.large)
    fill(colors.yellow)
    text(texts.title, board.center, board.size * 0.25)
    // menu items
    fill(colors.dark)
    for (var i = 0; i < texts.menu.length; i++) {
      noStroke()
      textSize(texts.medium)
      text(texts.menu[i], board.center, board.size * 0.5 + board.size * 0.2 * i)
    }
    // start
    if (states.item === 0) {
      strokeWeight(board.size * 0.025)
      stroke(colors.dark)
      fill(colors.green)
      text(texts.menu[0], board.center, board.size * 0.5)
    }
    // about
    if (states.item === 1) {
      strokeWeight(board.size * 0.025)
      stroke(colors.dark)
      fill(colors.green)
      text(texts.menu[1], board.center, board.size * 0.7)
    }
    // credit
    noStroke()
    fill(colors.dark)
    textSize(texts.small)
    text(texts.credit, board.center, board.size * 0.9)
  }
  // levels
  if (states.levels === true) {
    for (var i = 0; i < sqrt(levels.levels.length); i++) {
      for (var j = 0; j < sqrt(levels.levels.length); j++) {
        // selected level
        if (levels.selection[i][j] === 1) {
          fill(colors.green)
        } else {
          // solved level
          if (levels.state[i][j] === 1) {
            fill(colors.yellow)
            // unsolved level
          } else {
            fill(colors.blue)
          }
        }
        stroke(colors.dark)
        strokeWeight(board.size * 0.025)
        rect(board.center + (i - 1) * board.size * 0.25, board.center + (j - 1) * board.size * 0.25, board.size * 0.2, board.size * 0.2, board.size * 0.05)
        noStroke()
        fill(colors.dark)
        ellipse(board.center + (i - 1) * board.size * 0.25 + board.size * 0.05, board.center + (j - 1) * board.size * 0.25 - board.size * 0.025, board.size * 0.04, board.size * 0.04)
        ellipse(board.center + (i - 1) * board.size * 0.25 - board.size * 0.05, board.center + (j - 1) * board.size * 0.25 - board.size * 0.025, board.size * 0.04, board.size * 0.04)
        if (levels.selection[i][j] === 1) {
          // neutral smiley
          noStroke()
          fill(colors.dark)
          rect(board.center + (i - 1) * board.size * 0.25, board.center + (j - 1) * board.size * 0.25 + board.size * 0.0375, board.size * 0.1, board.size * 0.025, board.size * 0.05)
        } else {
          if (levels.state[i][j] === 0) {
            // sad smiley
            noFill()
            stroke(colors.dark)
            strokeWeight(board.size * 0.025)
            arc(board.center + (i - 1) * board.size * 0.25, board.center + (j - 1) * board.size * 0.25 + board.size * 0.075, board.size * 0.1, board.size * 0.1, 5 * PI / 4, -PI / 4)
          } else {
            // happy smiley
            noFill()
            stroke(colors.dark)
            strokeWeight(board.size * 0.025)
            arc(board.center + (i - 1) * board.size * 0.25, board.center + (j - 1) * board.size * 0.25, board.size * 0.1, board.size * 0.1, PI / 4, (3 / 4) * PI)
          }
        }
      }
    }
    // quit levels button
    fill(colors.dark)
    stroke(colors.dark)
    strokeWeight(board.size * 0.025)
    rect(board.center, board.center, board.size * 0.2, board.size * 0.2, board.size * 0.05)
    if (levels.selection[1][1] === 1) {
      stroke(colors.dark)
      strokeWeight(board.size * 0.025)
      fill(colors.green)
      rect(board.center, board.center, board.size * 0.2, board.size * 0.2, board.size * 0.05)
    }
  }
  // about
  if (states.about === true) {
    fill(colors.dark)
    noStroke()
    textSize(texts.small * 0.75)
    text(texts.about, board.center, board.size * 0.15)
  }
  // play
  if (states.play === true) {
    // draw smileys
    for (var i = 0; i < levels.levels[levels.active].length; i++) {
      if (levels.states[levels.active][i] === 0) {
        fill(colors.yellow)
      }
      if (levels.states[levels.active][i] === 1) {
        fill(colors.green)
      }
      if (levels.states[levels.active][i] === 2) {
        fill(colors.blue)
      }
      stroke(colors.dark)
      strokeWeight(board.size * 0.025 * (10 / (levels.active + 10)))
      push()
      translate(board.center, board.center)
      rotate(2 * Math.PI / (levels.levels[levels.active].length / i))
      translate(board.size * 0.3, 0)
      rect(0, 0, board.size * 0.2 * (10 / (levels.active + 10)), board.size * 0.2 * (10 / (levels.active + 10)), board.size * 0.05 * (10 / (levels.active + 10)))
      noStroke()
      fill(colors.dark)
      ellipse(board.size * 0.025 * (10 / (levels.active + 10)), -board.size * 0.035 * (10 / (levels.active + 10)), board.size * 0.04 * (10 / (levels.active + 10)), board.size * 0.04 * (10 / (levels.active + 10)))
      ellipse(board.size * 0.025 * (10 / (levels.active + 10)), board.size * 0.035 * (10 / (levels.active + 10)), board.size * 0.04 * (10 / (levels.active + 10)), board.size * 0.04 * (10 / (levels.active + 10)))
      if (levels.states[levels.active][i] === 0) {
        // happy smiley
        noFill()
        stroke(colors.dark)
        strokeWeight(board.size * 0.025 * (10 / (levels.active + 10)))
        arc(-board.size * 0.01 * (10 / (levels.active + 10)), 0, board.size * 0.1 * (10 / (levels.active + 10)), board.size * 0.1 * (10 / (levels.active + 10)), 3 * PI / 4, -3 * PI / 4)
      }
      if (levels.states[levels.active][i] === 1) {
        // neutral smiley
        fill(colors.dark)
        noStroke()
        strokeWeight(board.size * 0.025 * (10 / (levels.active + 10)))
        rect(-board.size * 0.045 * (10 / (levels.active + 10)), 0, board.size * 0.025 * (10 / (levels.active + 10)), board.size * 0.1 * (10 / (levels.active + 10)), board.size * 0.01 * (10 / (levels.active + 10)))
      }
      if (levels.states[levels.active][i] === 2) {
        // sad smiley
        noFill()
        stroke(colors.dark)
        strokeWeight(board.size * 0.025 * (10 / (levels.active + 10)))
        arc(-board.size * 0.08 * (10 / (levels.active + 10)), 0, board.size * 0.1 * (10 / (levels.active + 10)), board.size * 0.1 * (10 / (levels.active + 10)), 7 * PI / 4, 1 * PI / 4)
      }
      pop()
    }
    // first smiley selected
    if (states.first !== -1) {
      noFill()
      if (frameCount % 60 < 30) {
        stroke(colors.yellow)
      } else {
        stroke(colors.purple)
      }
      strokeWeight(board.size * 0.02 * (10 / (levels.active + 10)))
      push()
      translate(board.center, board.center)
      rotate(2 * Math.PI / (levels.levels[levels.active].length / states.first))
      translate(board.size * 0.3, 0)
      rect(0, 0, board.size * 0.25 * (10 / (levels.active + 10)), board.size * 0.25 * (10 / (levels.active + 10)), board.size * 0.075 * (10 / (levels.active + 10)))
      pop()
    }
    // second smiley selected
    if (states.second !== -1) {
      noFill()
      if (frameCount % 60 < 30) {
        stroke(colors.purple)
      } else {
        stroke(colors.yellow)
      }
      strokeWeight(board.size * 0.02 * (10 / (levels.active + 10)))
      push()
      translate(board.center, board.center)
      rotate(2 * Math.PI / (levels.levels[levels.active].length / states.second))
      translate(board.size * 0.3, 0)
      rect(0, 0, board.size * 0.25 * (10 / (levels.active + 10)), board.size * 0.25 * (10 / (levels.active + 10)), board.size * 0.075 * (10 / (levels.active + 10)))
      pop()
    }
    // smiley in focus
    if (states.focus < levels.levels[levels.active].length) {
      noFill()
      stroke(colors.dark)
      strokeWeight(board.size * 0.01 * (10 / (levels.active + 10)))
      push()
      translate(board.center, board.center)
      rotate(2 * Math.PI / (levels.levels[levels.active].length / states.focus))
      translate(board.size * 0.3, 0)
      rect(0, 0, board.size * 0.25 * (10 / (levels.active + 10)), board.size * 0.25 * (10 / (levels.active + 10)), board.size * 0.075 * (10 / (levels.active + 10)))
      pop()
    }
    // quit play button
    if (states.focus === levels.levels[levels.active].length) {
      fill(colors.green)
    } else {
      fill(colors.dark)
    }
    stroke(colors.dark)
    strokeWeight(board.size * 0.025 * (10 / (levels.active + 10)))
    ellipse(board.center, board.center, board.size * 0.35 - board.size * 0.1 * (10 / (levels.active + 10)), board.size * 0.35 - board.size * 0.1 * (10 / (levels.active + 10)))
    // solved smiley button
    if (states.solved === true) {
      if ((frameCount % 16) < 8) {
        fill(colors.yellow)
      } else {
        fill(colors.green)
      }
      stroke(colors.dark)
      strokeWeight(board.size * 0.025 * (10 / (levels.active + 10)))
      ellipse(board.center, board.center, board.size * 0.35 - board.size * 0.1 * (10 / (levels.active + 10)), board.size * 0.35 - board.size * 0.1 * (10 / (levels.active + 10)))
    }
  }
}

function keyPressed() {
  // menu
  if (states.menu === true) {
    // skip to next item -- right and down arrow
    if (keyCode === 39 || keyCode === 40) {
      playSound(sounds.skip)
      states.item++
      if (states.item > texts.menu.length - 1) {
        states.item = 0
      }
    }
    // skip to previous item -- left and up arrow
    if (keyCode === 37 || keyCode === 38) {
      playSound(sounds.skip)
      states.item--
      if (states.item < 0) {
        states.item = texts.menu.length - 1
      }
    }
    // enter levels -- space
    if (keyCode === 32 && states.item === 0) {
      playSound(sounds.enter)
      setTimeout(function() {
        states.menu = false;
        states.levels = true
      }, 1)
      levels.selected = [1, 1]
      levels.selection[levels.selected[0]][levels.selected[1]] = 1
    }
    // enter about -- space
    if (keyCode === 32 && states.item === 1) {
      playSound(sounds.enter)
      states.menu = false
      setTimeout(function() {
        states.about = true
      }, 1)
    }
  }
  // levels
  if (states.levels === true) {
    // right arrow
    if (keyCode === 39) {
      playSound(sounds.skip)
      levels.selected[0]++
      if (levels.selected[0] > levels.selection.length - 1) {
        levels.selected[0] = 0
        levels.selection[levels.selected[0]][levels.selected[1]] = 1
        levels.selection[levels.selection.length - 1][levels.selected[1]] = 0
      } else {
        levels.selection[levels.selected[0]][levels.selected[1]] = 1
        levels.selection[levels.selected[0] - 1][levels.selected[1]] = 0
      }
    }
    // down arrow
    if (keyCode === 40) {
      playSound(sounds.skip)
      levels.selected[1]++
      if (levels.selected[1] > levels.selection.length - 1) {
        levels.selected[1] = 0
        levels.selection[levels.selected[0]][levels.selected[1]] = 1
        levels.selection[levels.selected[0]][levels.selection.length - 1] = 0
      } else {
        levels.selection[levels.selected[0]][levels.selected[1]] = 1
        levels.selection[levels.selected[0]][levels.selected[1] - 1] = 0
      }
    }
    // left arrow
    if (keyCode === 37) {
      playSound(sounds.skip)
      levels.selected[0]--
      if (levels.selected[0] < 0) {
        levels.selected[0] = levels.selection.length - 1
        levels.selection[levels.selected[0]][levels.selected[1]] = 1
        levels.selection[0][levels.selected[1]] = 0
      } else {
        levels.selection[levels.selected[0]][levels.selected[1]] = 1
        levels.selection[levels.selected[0] + 1][levels.selected[1]] = 0
      }
    }
    // up arrow
    if (keyCode === 38) {
      playSound(sounds.skip)
      levels.selected[1]--
      if (levels.selected[1] < 0) {
        levels.selected[1] = levels.selection.length - 1
        levels.selection[levels.selected[0]][levels.selected[1]] = 1
        levels.selection[levels.selected[0]][0] = 0
      } else {
        levels.selection[levels.selected[0]][levels.selected[1]] = 1
        levels.selection[levels.selected[0]][levels.selected[1] + 1] = 0
      }
    }
    // quit levels or enter play-- space
    if (keyCode === 32) {
      if (levels.selected[0] === 1 && levels.selected[1] === 1) {
        playSound(sounds.quit)
        states.menu = true
        states.levels = false
        states.play = false
        levels.selection[1][1] = 1
        levels.selected = [1, 1]
        levels.active = null
      } else {
        playSound(sounds.enter)
        setTimeout(function() {
          states.play = true;
          states.levels = false
          states.first = -1
          states.second = -1
        }, 1)
        if (levels.selected[1] * levels.selection.length + levels.selected[0] <= 4) {
          levels.active = levels.selected[1] * levels.selection.length + levels.selected[0]
        } else {
          levels.active = levels.selected[1] * levels.selection.length + levels.selected[0] - 1
        }
        states.focus = levels.levels[levels.active].length
        levels.states[levels.active] = makeStates(levels.levels[levels.active])
      }
    }
  }
  // about
  if (states.about === true) {
    // quit about
    if (keyCode === 32) {
      playSound(sounds.quit)
      states.menu = true
      states.about = false
    }
  }
  // play
  if (states.play === true) {
    // skip to next smiley -- right or down arrow
    if (keyCode === 39 || keyCode === 40) {
      playSound(sounds.skip)
      states.focus++
      if (states.focus > levels.levels[levels.active].length - 1) {
        states.focus = 0
      }
    }
    // skip to prevoius smiley -- left or up arrow
    if (keyCode === 37 || keyCode === 38) {
      playSound(sounds.skip)
      states.focus--
      if (states.focus < 0) {
        states.focus = levels.levels[levels.active].length - 1
      }
    }
    // select the first smiley -- space
    if (keyCode === 32) {
      playSound(sounds.select)
      frameCount = 0
      if (states.focus < levels.levels[levels.active].length) {
        if (states.first === -1) {
          states.first = states.focus
        }
      }
    }
    // select the second smiley + swap them + checking solvedness -- space
    if (keyCode === 32 && states.first !== states.focus) {
      playSound(sounds.select)
      frameCount = 0
      if (states.focus < levels.levels[levels.active].length) {
        if (states.focus !== states.first) {
          if (states.second === -1) {
            states.second = states.focus
          }
        }
      }
      // swap the two selected items
      if (states.first !== -1 && states.second !== -1) {
        playSound(sounds.swap)
        // reset selection after swapping
        setTimeout(function() {
          states.focus = levels.levels[levels.active].length
        }, 1)
        setTimeout(function() {
          levels.temp = levels.levels[levels.active][states.first]
          levels.levels[levels.active][states.first] = levels.levels[levels.active][states.second]
          levels.levels[levels.active][states.second] = levels.temp
          levels.temp = null
          levels.states[levels.active] = makeStates(levels.levels[levels.active])
          states.first = -1
          states.second = -1
          // check if it's solved or not
          if (levels.states[levels.active].flat().every(checkArray) === true) {
            states.solved = true
            playSound(sounds.solved)
          }
          // return after solved
          if (states.solved === true) {
            frameCount = 0
            setTimeout(function() {
              states.play = false
              states.levels = true
              states.solved = false
              if (levels.selected[1] * levels.selection.length + levels.selected[0] <= 4) {
                levels.state[levels.active % levels.state.length][floor(levels.active / levels.state.length)] = 1
              } else {
                levels.state[(levels.active + 1) % levels.state.length][floor((levels.active + 1) / levels.state.length)] = 1
              }
              levels.selection[levels.selected[0]][levels.selected[1]] = 0
              levels.selected = [1, 1]
              levels.selection[levels.selected[0]][levels.selected[1]] = 1
            }, 2000)
          }
        }, 2000)
      }
    }
    // quit play -- space
    if (keyCode === 32 && states.focus === levels.levels[levels.active].length) {
      playSound(sounds.quit)
      states.levels = true
      states.play = false
      if (levels.selected[1] * levels.selection.length + levels.selected[0] <= 4) {
        levels.state[levels.active % levels.state.length][floor(levels.active / levels.state.length)] = 0
      } else {
        levels.state[(levels.active + 1) % levels.state.length][floor((levels.active + 1) / levels.state.length)] = 0
      }
      levels.selection[levels.selected[0]][levels.selected[1]] = 0
      levels.selected = [1, 1]
      levels.selection[levels.selected[0]][levels.selected[1]] = 1
    }
  }
}

function checkArray(a) {
  return a < 1
}

function makeStates(array) {
  var stateOf = create1DArray(array.length, 0)
  var copyOf = create1DArray(array.length + 2, 0)
  copyOf[0] = array[array.length - 1]
  copyOf[array.length + 1] = array[0]
  for (var i = 0; i < array.length; i++) {
    copyOf[1 + i] = array[i]
  }
  for (var i = 1; i < copyOf.length - 1; i++) {
    if (copyOf[i] === 0) {
      // checking if the previous item is the neighbour of zero or not
      if (copyOf[i] === (copyOf[i - 1] + 1) % array.length || copyOf[i] === copyOf[i - 1] - 1) {
        stateOf[i - 1] = stateOf[i - 1]
      } else {
        stateOf[i - 1]++
      }
      // checking if the next item is the neighbour of zero or not
      if (copyOf[i] === copyOf[i + 1] - 1 || copyOf[i] === (copyOf[i + 1] + 1) % array.length) {
        stateOf[i - 1] = stateOf[i - 1]
      } else {
        stateOf[i - 1]++
      }
    }
    if (copyOf[i] > 0 && copyOf[i] < array.length - 1) {
      // checking if the prevoius item is the neighbour of the current one or not
      if (copyOf[i] === copyOf[i - 1] + 1 || copyOf[i] === copyOf[i - 1] - 1) {
        stateOf[i - 1] = stateOf[i - 1]
      } else {
        stateOf[i - 1]++
      }
      // checking if the next item is the neighbour of the current one or not
      if (copyOf[i] === copyOf[i + 1] - 1 || copyOf[i] === copyOf[i + 1] + 1) {
        stateOf[i - 1] = stateOf[i - 1]
      } else {
        stateOf[i - 1]++
      }
    }
    if (copyOf[i] === array.length - 1) {
      // checking if the previous item is the neighbour of last item or not
      if (copyOf[i] === copyOf[i - 1] + 1 || copyOf[i] === (array.length - copyOf[i - 1] - 1) % array.length) {
        stateOf[i - 1] = stateOf[i - 1]
      } else {
        stateOf[i - 1]++
      }
      // checking if the next item is the neighbour of last item or not
      if (copyOf[i] === copyOf[i + 1] + 1 || copyOf[i] === (array.length - copyOf[i + 1] - 1) % array.length) {
        stateOf[i - 1] = stateOf[i - 1]
      } else {
        stateOf[i - 1]++
      }
    }
  }
  return stateOf
}

function playSound(s) {
  if (sounds.bool === true) {
    s.play()
  }
}

function create1DArray(num, init) {
  var array = []
  for (var i = 0; i < num; i++) {
    array.push(init)
  }
  return array
}

function create2DArray(numRows, numCols, init, bool) {
  var array = []
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}

function windowResized() {
  createCanvas(windowWidth, windowHeight)
  board.size = round(windowHeight * 0.9)
  board.center = round(windowHeight * 0.9 * 0.5)
  texts.large = round(windowHeight * 0.175)
  texts.medium = round(windowHeight * 0.1)
  texts.small = round(windowHeight * 0.05)
}
